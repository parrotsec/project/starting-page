export interface DocumentationData {
  imgSrc: string;
  alt: string;
  href: string;
  label: string;
}

const documentationData: DocumentationData[] = [
  {
    imgSrc: "../../assets/documentation/installation.png",
    alt: "Install ParrotOS",
    href: "https://parrotsec.org/docs/installation",
    label: "Installation",
  },
  {
    imgSrc: "../../assets/documentation/virtualization.png",
    alt: "Virtualization",
    href: "https://parrotsec.org/docs/category/virtualization",
    label: "Virtualization",
  },
  {
    imgSrc: "../../assets/documentation/configuration.png",
    alt: "Configuration",
    href: "https://parrotsec.org/docs/category/configuration",
    label: "Configuration",
  },
  {
    imgSrc: "../../assets/documentation/wsl.png",
    alt: "WSL Edition",
    href: "https://parrotsec.org/docs/installation/install-with-wsl",
    label: "WSL Edition",
  },
  {
    imgSrc: "../../assets/documentation/raspberrypi.png",
    alt: "Raspberry Pi Edition",
    href: "https://parrotsec.org/docs/installation/raspberrypi",
    label: "Raspberry Pi",
  },
];

export default documentationData;
