import { useState } from "react";
import { motion } from "framer-motion";

import Footer from "./components/Footer";
import Menu from "./components/Menu";
import SearchBar from "./components/SearchBar";
import Contribute from "./components/Contribute";
import Documentation from "./components/Documentation";

import { SearchEngine } from "./types";

const App = () => {
  const [currentEngine, setCurrentEngine] =
    useState<SearchEngine>("DuckDuckGo");

  const handleTabChange = (engine: SearchEngine) => {
    setCurrentEngine(engine);
  };

  const handleSearch = (query: string, engine: SearchEngine) => {
    const encodedQuery = encodeURIComponent(query);
    let searchUrl = "";

    if (engine === "Google") {
      searchUrl = `https://www.google.com/search?q=${encodedQuery}`;
    } else if (engine === "DuckDuckGo") {
      searchUrl = `https://duckduckgo.com/?q=${encodedQuery}`;
    }

    if (searchUrl) {
      window.open(searchUrl, "_blank");
    }
  };

  return (
    <div className="container mx-auto">
      <Menu />
      <div className="h-full mt-16 bg-base-100">
        <div className="hero-content text-center">
          <div className="w-full">
            <div className="sm:p-12 w-full">
              <div className="divider divider-accent my-10">
                Pick a search engine
              </div>
              <div role="tablist" className="tabs-lg tabs-lifted">
                <motion.a
                  role="tab"
                  className={`tab w-48 ${
                    currentEngine === "Google"
                      ? "bg-secondary text-accent"
                      : "tab-active"
                  }`}
                  onClick={() => handleTabChange("Google")}
                  whileTap={{ scale: 0.95 }}
                >
                  Google
                </motion.a>
                <motion.a
                  role="tab"
                  className={`tab w-64 ${
                    currentEngine === "DuckDuckGo"
                      ? "bg-secondary text-accent"
                      : "tab-active"
                  }`}
                  onClick={() => handleTabChange("DuckDuckGo")}
                  whileTap={{ scale: 0.95 }}
                >
                  DuckDuckGo
                </motion.a>
              </div>
              <SearchBar
                currentEngine={currentEngine}
                onSearch={handleSearch}
              />
            </div>
          </div>
        </div>
      </div>
      <Documentation />
      <Contribute />
      <Footer />
    </div>
  );
};

export default App;
