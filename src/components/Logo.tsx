import Logo from "../../assets/logo.png";

const Parrot = () => {
  return <img className="w-10 h-10" src={Logo} alt="Parrot on Docker" />;
};

export default Parrot;
