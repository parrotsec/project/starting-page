import ParrotLogo from "../../assets/logo.png";

const Footer = () => {
  return (
    <>
      <footer className="footer p-10 bg-base-200 text-base-content">
        <aside>
          <img className="w-12 h-12" src={ParrotLogo} alt="Parrot on Docker" />
          <p>
            ParrotSec
            <br />
            <span className="font-bold">
              The ultimate framework for your Cyber Security operations
            </span>
          </p>
        </aside>
        <nav>
          <h6 className="footer-title">Resources</h6>
          <a className="link link-hover" href="https://parrotsec.org/docs/">
            Documentation
          </a>
          <a
            className="link link-hover"
            href="https://parrotsec.org/community/"
          >
            Community
          </a>
          <a className="link link-hover" href="https://parrotsec.org/blog/">
            Blog
          </a>
        </nav>
        <nav>
          <h6 className="footer-title">About Us</h6>
          <a className="link link-hover" href="https://parrotsec.org/team/">
            Team
          </a>
          <a className="link link-hover" href="https://parrotsec.org/partners/">
            Partners
          </a>
          <a className="link link-hover" href="https://parrotsec.org/donate/">
            Donate
          </a>
          <a className="link link-hover" href="https://parrotsec.org/careers/">
            Careers
          </a>
        </nav>
        <nav>
          <h6 className="footer-title">Social</h6>
          <a
            className="link link-hover"
            href="https://www.facebook.com/ParrotSec/"
          >
            Facebook
          </a>
          <a className="link link-hover" href="https://twitter.com/parrotsec">
            Twitter
          </a>
          <a
            className="link link-hover"
            href="https://www.instagram.com/parrotproject/"
          >
            Instagram
          </a>
          <a className="link link-hover" href="https://discord.gg/j7QTaCzAsm">
            Discord
          </a>
          <a className="link link-hover" href="https://t.me/parrotsecgroup">
            Telegram
          </a>
          <a
            className="link link-hover"
            href="https://www.linkedin.com/company/parrotsec/about/"
          >
            LinkedIn
          </a>
          <a
            className="link link-hover"
            href="https://www.reddit.com/r/ParrotSecurity/"
          >
            Reddit
          </a>
        </nav>
      </footer>
    </>
  );
};

export default Footer;
