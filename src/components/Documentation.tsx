import documentationData, {
  DocumentationData,
} from "../data/documentationData";

interface CardProps {
  data: DocumentationData;
}

const Card = ({ data }: CardProps) => (
  <div className="carousel-item">
    <div className="card w-96 bg-base-100 shadow-xl">
      <figure className="px-10 pt-10">
        <img
          src={data.imgSrc}
          alt={data.alt}
          className="rounded-xl max-h-full"
        />
      </figure>
      <div className="card-body items-center text-center">
        <div className="card-actions">
          <a
            href={data.href}
            target="_blank"
            rel="noopener noreferrer"
            className="btn btn-secondary"
          >
            {data.label}
          </a>
        </div>
      </div>
    </div>
  </div>
);

const Documentation = () => {
  return (
    <div className="sm:p-12 mb-24">
      <h1 className="sm:text-7xl text-4xl text-center font-bold text-neutral pb-12">
        User Documentation
      </h1>
      <div className="carousel carousel-center w-full p-4 space-x-4 bg-secondary rounded-box">
        {documentationData.map((data, index) => (
          <Card key={index} data={data} />
        ))}
      </div>
    </div>
  );
};

export default Documentation;
