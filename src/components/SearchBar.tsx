import { useState, useEffect } from "react";
import { SearchEngine } from "../types";

interface SearchBarProps {
  currentEngine: SearchEngine;
  onSearch: (query: string, engine: SearchEngine) => void;
}

const SearchBar = ({ currentEngine, onSearch }: SearchBarProps) => {
  const [query, setQuery] = useState("");

  const handleSearch = () => {
    if (query.trim() !== "") {
      onSearch(query, currentEngine);
    }
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (event.key === "Enter") {
      handleSearch();
    }
  };

  useEffect(() => {
    setQuery("");
  }, [currentEngine]);

  return (
    <div className="w-full">
      <div className="search-bar my-12 p-2">
        <div className="join">
          <label className="input input-bordered md:select-lg join-item flex items-center">
            <input
              type="text"
              className="join-item text-lg md:w-96"
              value={query}
              onChange={(e) => setQuery(e.target.value)}
              onKeyDown={handleKeyDown}
              placeholder="Enter your query"
            />
          </label>
          <button
            className="btn btn-outline btn-accent join-item md:select-lg"
            onClick={handleSearch}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="24"
              height="24"
              fill="currentColor"
              viewBox="0 0 16 16"
            >
              <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001q.044.06.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1 1 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0" />
            </svg>
          </button>
        </div>
      </div>
    </div>
  );
};

export default SearchBar;
