const Contribute = () => {
  return (
    <div className="sm:p-12 mb-24">
      <h1 className="sm:text-7xl text-4xl text-center font-bold text-neutral">
        <span className="text-accent">Contribute</span> to the Parrot Project
      </h1>
      <h2 className="text-lg text-center mt-8">
        ParrotOS was born as a fully open source project, anyone can see what is
        inside.
      </h2>
      <div className="flex w-full lg:flex-row pt-12">
        <a
          href="https://gitlab.com/parrotsec"
          className="block w-full"
          target="_blank"
          rel="noopener noreferrer"
        >
          <div className="grid h-20 flex-grow card bg-base-300 hover:bg-primary rounded-box place-items-center">
            <div className="flex items-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                fill="currentColor"
                viewBox="0 0 16 16"
                className="mr-2"
              >
                <path d="m15.734 6.1-.022-.058L13.534.358a.57.57 0 0 0-.563-.356.6.6 0 0 0-.328.122.6.6 0 0 0-.193.294l-1.47 4.499H5.025l-1.47-4.5A.572.572 0 0 0 2.47.358L.289 6.04l-.022.057A4.044 4.044 0 0 0 1.61 10.77l.007.006.02.014 3.318 2.485 1.64 1.242 1 .755a.67.67 0 0 0 .814 0l1-.755 1.64-1.242 3.338-2.5.009-.007a4.05 4.05 0 0 0 1.34-4.668Z" />
              </svg>
              <span className="text-2xl">GitLab</span>
            </div>
          </div>
        </a>
        <div className="divider divider-accent divider-horizontal"></div>
        <a
          href="https://github.com/ParrotSec"
          className="block w-full"
          target="_blank"
          rel="noopener noreferrer"
        >
          <div className="grid h-20 flex-grow card bg-base-300 hover:bg-primary rounded-box place-items-center">
            <div className="flex items-center">
              <svg
                xmlns="http://www.w3.org/2000/svg"
                width="24"
                height="24"
                fill="currentColor"
                viewBox="0 0 16 16"
                className="mr-2"
              >
                <path d="M8 0C3.58 0 0 3.58 0 8c0 3.54 2.29 6.53 5.47 7.59.4.07.55-.17.55-.38 0-.19-.01-.82-.01-1.49-2.01.37-2.53-.49-2.69-.94-.09-.23-.48-.94-.82-1.13-.28-.15-.68-.52-.01-.53.63-.01 1.08.58 1.23.82.72 1.21 1.87.87 2.33.66.07-.52.28-.87.51-1.07-1.78-.2-3.64-.89-3.64-3.95 0-.87.31-1.59.82-2.15-.08-.2-.36-1.02.08-2.12 0 0 .67-.21 2.2.82.64-.18 1.32-.27 2-.27s1.36.09 2 .27c1.53-1.04 2.2-.82 2.2-.82.44 1.1.16 1.92.08 2.12.51.56.82 1.27.82 2.15 0 3.07-1.87 3.75-3.65 3.95.29.25.54.73.54 1.48 0 1.07-.01 1.93-.01 2.2 0 .21.15.46.55.38A8.01 8.01 0 0 0 16 8c0-4.42-3.58-8-8-8" />
              </svg>
              <span className="text-2xl">GitHub</span>
            </div>
          </div>
        </a>
      </div>
    </div>
  );
};

export default Contribute;
